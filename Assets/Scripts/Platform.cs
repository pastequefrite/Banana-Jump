﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public float speed = 0.1f;
    public int jumpBeforeBreak = -1;
    public bool isBreakable = false;

    Camera mainCamera;
    bool moveDown = false;

    void Awake()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (moveDown)
        {
            // TODO Increase Speed with GameScore rate
            transform.Translate(Vector2.down * speed * Time.deltaTime);
        }

        Vector3 camPos = mainCamera.WorldToViewportPoint(transform.position);
        if (camPos.y < 0f)
        {
            DestroyObject();
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        GameObject collidedObject = collision.gameObject;
        if (collidedObject.CompareTag("Player")) {

            // Remove platform only if player is grounded on it before
            Player player = collidedObject.GetComponent<Player>();
            if (player.GetGrounded())
            {
                jumpBeforeBreak--;
                if (isBreakable && jumpBeforeBreak < 1)
                {
                    DestroyObject();
                }
            }
        }
    }

    void DestroyObject()
    {
        // Prevent Game Over behaviour
        transform.DetachChildren();
        Destroy(gameObject);
    }

    public bool GetMoveDown()
    {
        return moveDown;
    }

    public void SetMoveDown(bool moveDown)
    {
        this.moveDown = moveDown;
    }


}

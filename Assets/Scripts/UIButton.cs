﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIButton : MonoBehaviour
{
    Button button;
    AudioManager am;

    private void Awake()
    {
        button = gameObject.GetComponent<Button>();
        am = FindObjectOfType<AudioManager>();
    }


    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        am.Play("Click");
    }
}
